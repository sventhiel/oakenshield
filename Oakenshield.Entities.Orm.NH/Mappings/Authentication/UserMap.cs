﻿using FluentNHibernate.Mapping;
using Oakenshield.Entities.Authentication;

namespace Oakenshield.Entities.Orm.NH.Mappings.Authentication
{
    public class UserMap : ClassMap<User>
    {
        public UserMap()
        {
            Id(x => x.Id);

            HasManyToMany(x => x.Groups)
                .Cascade.All()
                .Table("Users_Groups");

            Map(x => x.Name);

            HasMany(x => x.Persons)
                .Inverse()
                .Cascade.All();
        }
    }
}