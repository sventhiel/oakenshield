﻿using FluentNHibernate.Mapping;
using Oakenshield.Entities.Subjects;

namespace Oakenshield.Entities.Orm.NH.Mappings.Subjects
{
    public class PersonMap : SubclassMap<Person>
    {
        public PersonMap()
        {
            References(x => x.User)
                .Column("UserRef")
                .Cascade.All();
        }
    }
}