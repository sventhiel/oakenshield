﻿using FluentNHibernate.Mapping;
using Oakenshield.Entities.Subjects;

namespace Oakenshield.Entities.Orm.NH.Mappings.Subjects
{
    public class SubjectMap : ClassMap<Subject>
    {
        public SubjectMap()
        {
            Id(x => x.Id);

            Map(x => x.Name);

            HasMany(x => x.Permissions)
                .Inverse()
                .Cascade.All();
        }
    }
}