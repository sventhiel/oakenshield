﻿using FluentNHibernate.Mapping;
using Oakenshield.Entities.Subjects;

namespace Oakenshield.Entities.Orm.NH.Mappings.Subjects
{
    public class GroupMap : SubclassMap<Group>
    {
        public GroupMap()
        {
            HasManyToMany(x => x.Users)
                .Cascade.All()
                .Inverse()
                .Table("Users_Groups");
        }
    }
}