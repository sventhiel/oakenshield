﻿using FluentNHibernate.Mapping;
using Oakenshield.Entities.Objects;

namespace Oakenshield.Entities.Orm.NH.Mappings.Objects
{
    public class EntityMap : ClassMap<Entity>
    {
        public EntityMap()
        {
            Id(x => x.Id);

            Map(x => x.Assembly);
            Map(x => x.Name);
            Map(x => x.Namespace);

            HasMany(x => x.EntityPermissions)
                .Inverse()
                .Cascade.All();
        }
    }
}