﻿using FluentNHibernate.Mapping;
using Oakenshield.Entities.Objects;

namespace Oakenshield.Entities.Orm.NH.Mappings.Objects
{
    public class FeatureMap : ClassMap<Feature>
    {
        public FeatureMap()
        {
            Id(x => x.Id);

            HasMany(x => x.Children)
                .Inverse()
                .Cascade.All();

            Map(x => x.Name);

            References(x => x.Parent)
                .Column("ParentRef")
                .Cascade.All();
        }
    }
}