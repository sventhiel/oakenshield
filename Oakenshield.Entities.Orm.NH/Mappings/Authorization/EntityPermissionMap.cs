﻿using FluentNHibernate.Mapping;
using Oakenshield.Entities.Authorization;

namespace Oakenshield.Entities.Orm.NH.Mappings.Authorization
{
    public class EntityPermissionMap : SubclassMap<EntityPermission>
    {
        public EntityPermissionMap()
        {
            References(x => x.Entity)
                .Column("EntityRef")
                .Cascade.All();

            Map(x => x.ReferenceId);
            Map(x => x.RightType);
        }
    }
}