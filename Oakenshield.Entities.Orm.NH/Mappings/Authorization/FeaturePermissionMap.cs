﻿using FluentNHibernate.Mapping;
using Oakenshield.Entities.Authorization;

namespace Oakenshield.Entities.Orm.NH.Mappings.Authorization
{
    public class FeaturePermissionMap : SubclassMap<FeaturePermission>
    {
        public FeaturePermissionMap()
        {
            References(x => x.Feature)
                .Column("FeatureRef")
                .Cascade.All();
        }
    }
}