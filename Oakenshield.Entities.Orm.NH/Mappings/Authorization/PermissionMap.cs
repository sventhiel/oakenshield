﻿using FluentNHibernate.Mapping;
using Oakenshield.Entities.Authorization;

namespace Oakenshield.Entities.Orm.NH.Mappings.Authorization
{
    public class PermissionMap : ClassMap<Permission>
    {
        public PermissionMap()
        {
            Id(x => x.Id);

            Map(x => x.PermissionType);

            References(x => x.Subject)
                .Column("SubjectRef")
                .Cascade.All();
        }
    }
}