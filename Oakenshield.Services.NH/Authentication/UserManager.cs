﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using Oakenshield.Services.Api.Authentication;

namespace Oakenshield.Services.NH.Authentication
{
    public class UserManager : IUserManager
    {
        public ISession Session { get; set; }

        public UserManager(ISessionFactory sessionFactory)
        {
            Session = sessionFactory.GetCurrentSession();
        }
    }
}
