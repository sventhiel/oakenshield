﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using Oakenshield.Entities.Subjects;
using Oakenshield.Persistence.NH;
using Oakenshield.Services.Api.Subjects;

namespace Oakenshield.Services.NH.Subjects
{
    public class SubjectManager : ISubjectManager
    {
        public Repository<Group> GroupRepository { get; }
        public Repository<Person> PersonRepository { get; } 

        public SubjectManager(ISessionFactory sessionFactory)
        {
            var uow = new UnitOfWork(sessionFactory);

            GroupRepository = new Repository<Group>(uow);
            PersonRepository = new Repository<Person>(uow);
        }

        public Group CreateGroup(string name)
        {
            var group = new Group()
            {
                Name = name
            };

            GroupRepository.Add(group);

            return group;
        }
    }
}
