﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using Oakenshield.Services.Api.Authorization;

namespace Oakenshield.Services.NH.Authorization
{
    public class PermissionManager : IPermissionManager
    {
        public ISession Session { get; set; }

        public PermissionManager(ISessionFactory sessionFactory)
        {
            Session = sessionFactory.GetCurrentSession();
        }
    }
}
