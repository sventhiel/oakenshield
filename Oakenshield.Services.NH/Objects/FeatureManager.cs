﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using Oakenshield.Services.Api.Objects;

namespace Oakenshield.Services.NH.Objects
{
    public class FeatureManager : IFeatureManager
    {
        public ISession Session { get; set; }

        public FeatureManager(ISessionFactory sessionFactory)
        {
            Session = sessionFactory.GetCurrentSession();
        }
    }
}
