﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using Oakenshield.Services.Api.Objects;

namespace Oakenshield.Services.NH.Objects
{
    public class EntityManager : IEntityManager
    {
        public ISession Session { get; set; }

        public EntityManager(ISessionFactory sessionFactory)
        {
            Session = sessionFactory.GetCurrentSession();
        }
    }
}
