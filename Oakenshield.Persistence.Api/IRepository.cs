﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Oakenshield.Persistence.Api
{
    public interface IRepository<T> where T : class
    {
        T Add(T entity);
        T Get(long id);
        T Get(Expression<Func<T, bool>> predicate);
        IQueryable<T> Query();
        IQueryable<T> Query(Expression<Func<T, bool>> predicate);
        T Remove(T entity);
    }
}
