﻿using Oakenshield.Entities.Authorization;
using Oakenshield.Entities.Common;
using System.Collections.Generic;

namespace Oakenshield.Entities.Subjects
{
    public abstract class Subject : BaseEntity
    {
        public virtual string Name { get; set; }
        public virtual ICollection<Permission> Permissions { get; set; }

        public Subject()
        {
            Permissions = new HashSet<Permission>();
        }
    }
}