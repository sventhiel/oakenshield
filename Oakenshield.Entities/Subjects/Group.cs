﻿using Oakenshield.Entities.Authentication;
using System.Collections.Generic;

namespace Oakenshield.Entities.Subjects
{
    public class Group : Subject
    {
        public virtual ICollection<User> Users { get; set; }

        public Group()
        {
            Users = new HashSet<User>();
        }
    }
}