﻿using Oakenshield.Entities.Authentication;

namespace Oakenshield.Entities.Subjects
{
    public class Person : Subject
    {
        public virtual User User { get; set; }
    }
}