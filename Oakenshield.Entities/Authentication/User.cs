﻿using Oakenshield.Entities.Common;
using Oakenshield.Entities.Subjects;
using System.Collections.Generic;

namespace Oakenshield.Entities.Authentication
{
    public class User : BaseEntity
    {
        public virtual ICollection<Group> Groups { get; set; }
        public virtual string Name { get; set; }
        public virtual ICollection<Person> Persons { get; set; }

        public User()
        {
            Groups = new HashSet<Group>();
            Persons = new HashSet<Person>();
        }
    }
}