﻿using Oakenshield.Entities.Objects;

namespace Oakenshield.Entities.Authorization
{
    public class EntityPermission : Permission
    {
        public virtual Entity Entity { get; set; }
        public virtual long ReferenceId { get; set; }
        public virtual RightType RightType { get; set; }
    }

    public enum RightType
    {
        Read = 0,
        Write = 1,
        Delete = 2
    }
}