﻿using Oakenshield.Entities.Objects;

namespace Oakenshield.Entities.Authorization
{
    public class FeaturePermission : Permission
    {
        public virtual Feature Feature { get; set; }
    }
}