﻿using Oakenshield.Entities.Common;
using Oakenshield.Entities.Subjects;

namespace Oakenshield.Entities.Authorization
{
    public abstract class Permission : BaseEntity
    {
        public virtual PermissionType PermissionType { get; set; }
        public virtual Subject Subject { get; set; }
    }

    public enum PermissionType
    {
        Deny = 0,
        Grant = 1
    }
}