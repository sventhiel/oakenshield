﻿using Oakenshield.Entities.Common;
using System.Collections.Generic;

namespace Oakenshield.Entities.Objects
{
    public class Feature : BaseEntity
    {
        public virtual ICollection<Feature> Children { get; set; }
        public virtual string Name { get; set; }
        public virtual Feature Parent { get; set; }

        public Feature()
        {
            Children = new HashSet<Feature>();
        }
    }
}