﻿using Oakenshield.Entities.Authorization;
using Oakenshield.Entities.Common;
using System.Collections.Generic;

namespace Oakenshield.Entities.Objects
{
    public class Entity : BaseEntity
    {
        public virtual string Assembly { get; set; }
        public virtual string Name { get; set; }
        public virtual string Namespace { get; set; }
        public virtual ICollection<EntityPermission> EntityPermissions { get; set; }

        public Entity()
        {
            EntityPermissions = new HashSet<EntityPermission>();
        }
    }
}