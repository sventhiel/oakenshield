﻿using Oakenshield.Entities.Common;

namespace Oakenshield.Entities.Objects
{
    public class Workflow : BaseEntity
    {
        public virtual Feature Feature { get; set; }
        public virtual string Name { get; set; }
    }
}