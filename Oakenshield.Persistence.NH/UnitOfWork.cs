﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using Oakenshield.Persistence.Api;

namespace Oakenshield.Persistence.NH
{
    public class UnitOfWork : IUnitOfWork
    {
        public ISession Session { get; private set; }
        private readonly ITransaction _transaction;

        public UnitOfWork(ISessionFactory sessionFactory)
        {
            Session = sessionFactory.OpenSession();
            _transaction = Session.BeginTransaction();
        }

        public void Dispose()
        {
            Session.Close();
            Session = null;
        }

        public void Commit()
        {
            if (_transaction.IsActive)
                _transaction.Commit();
        }

        public void Rollback()
        {
            if (_transaction.IsActive)
                _transaction.Rollback();
        }
    }
}
