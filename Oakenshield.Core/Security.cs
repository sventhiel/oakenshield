﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using Oakenshield.Entities.Orm.NH.Mappings.Authentication;

namespace Oakenshield.Core
{
    public class Security
    {
        public static ISessionFactory Configure()
        {
            return Fluently.Configure()
                .Database(PostgreSQLConfiguration.PostgreSQL82
                        .Raw("hbm2ddl.keywords", "none")
                        .ConnectionString(c => c.Is("Server=localhost;Port=5432;Database=Oakenshield;User Id=postgres;Password=proq3dm6;")))
                .Mappings(m => m.FluentMappings
                    .AddFromAssemblyOf<UserMap>())
                .ExposeConfiguration(CreateSchema)
                .BuildSessionFactory();
        }

        private static void CreateSchema(Configuration cfg)
        {
            var schemaExport = new SchemaExport(cfg);
            schemaExport.Drop(false, true);
            schemaExport.Create(false, true);
        }
    }
}