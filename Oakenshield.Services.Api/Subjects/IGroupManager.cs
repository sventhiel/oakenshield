﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Oakenshield.Entities.Subjects;

namespace Oakenshield.Services.Api.Subjects
{
    public interface IGroupManager
    {
        Group CreateGroup(string name);
    }
}
